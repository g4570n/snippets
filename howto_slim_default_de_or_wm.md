# HowTo: configurar el DE o WM por defecto en SLIM

Abrimos el fichero:

```shell_session
root@devuan:~# editor /etc/slim.conf
```

Una vez dentro, buscamos y editamos la siguiente linea para que quede así:

```slim
login_cmd	exec /bin/bash -login ~/.xinitrc %session
```

Luego hay que editar o crear el fichero `.xinitrc` en `/home/<usuario>`

Abrimos:

```shell_session
user@devuan:~$ editor ~/.xinitrc
```

y agregamos las siguientes lineas:

```conf
# Here Enlightenment is kept as default
session=${1:-enlightenment}
	
case $session in
	xfce)			exec startxfce4;;
	enlightenment)	exec enlightenment_start;;
	awesome)		exec awesome;;
# No known session, try to run it as command
	*)				exec $1;;
esac
```

En este ejemplo tengo instalados: Xfce, Awesome y Enlightenment, siendo este último el que se inicia por defecto. A menos que decida escojer alguno de los otros dos, precionando la tecla `F1`.

**Tip:** Para saber el `exec` exacto para cada DE o WM que tengas instalado, recomiendo revisar la carpeta: `/usr/share/xsessions/` allí encontrarás los ficheros con extencion `.desktop`, abriendolos con un editor de texto busca la linea `Exec=`.
